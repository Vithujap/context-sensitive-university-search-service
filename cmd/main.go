package main

import (
	"Assignment1/handler"
	"log"
	"net/http"
	"os"
)

func main() {

	//Define port
	port := os.Getenv("PORT")
	if port == "" {
		log.Println("$Port has not been set. Default: 8080")
		port = "8080"
	}

	//Assign path handlers for http server

	http.HandleFunc(handler.UNIINFO_PATH, handler.UniInfoHandler)
	http.HandleFunc(handler.NEIGHBOURUNIS_PATH, handler.NeighbourUnisHandler)
	http.HandleFunc(handler.DIAG_PATH, handler.DiagHandler)

	//Start HTTP server
	log.Println("Starting server on port " + port + "...")
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal(err.Error() + "Error in creating request")
	}

}
