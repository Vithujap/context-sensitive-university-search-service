package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func NeighbourUnisHandler(w http.ResponseWriter, r *http.Request) {
	//Use countryname from path to find border information
	//Use border information to ifnd the countries, and find universities in those countries
	switch r.Method {
	case http.MethodGet:
		//Prepare response header
		w.Header().Add("content-type", "application/json")
		//Splitting up the URL
		split := strings.Split(r.URL.Path, "/")

		//Check if the length of the split is 6. If it is, we add an empty string to the split so that the bad request (next if-statement) dont go out of bounds
		if len(split) == 6 {
			split = append(split, "")
		}

		if !(len(split) > 5 && (len(split) <= 7 && split[6] == "")) || split[5] == "" {
			http.Error(w, "Expecting format .../neighbourunis/{:country_name}/{:partial_or_complete_university_name}{?limit={:number}}", http.StatusBadRequest)
			return
		}

		//URL Variables
		countryName := split[4]
		universityName := split[5]
		limitQuery := r.URL.Query().Get("limit")
		limit, err := strconv.Atoi(limitQuery)
		if err != nil {
			fmt.Errorf("Error in response")
		}

		//Invoke external request to the restcountries API to get data
		resCountry, err := externalGetRequest(w, "https://restcountries.com/v3.1/name/"+url.PathEscape(countryName))
		if err != nil {
			fmt.Errorf(err.Error())
			return
		}

		//reading data from the resCountry http response
		bordersOutput, err := ioutil.ReadAll(resCountry.Body)
		if err != nil {
			http.Error(w, "Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)
			return
		}
		defer resCountry.Body.Close()

		var basisCountry []Country

		//Decoding JSON byte array into basisCountry struct
		json.Unmarshal(bordersOutput, &basisCountry)

		//check if the baseCountry array struct is not empty due to invalid country name
		if len(basisCountry) == 0 {
			fmt.Println("Error")
			return
		}

		//Converting the borderingcountries of the basecountry to a string to avoid invoking multiple requests on the restcountry API
		allBorderingCountries := strings.Join(basisCountry[0].Borders, ",")

		var borderingCountries []Country

		//Invoke external request to the restcountries API to get data
		resBorderingCountries, err := externalGetRequest(w, "https://restcountries.com/v3.1/alpha?codes="+url.PathEscape(allBorderingCountries))
		if err != nil {
			fmt.Errorf(err.Error())
			return
		}

		//reading data from the resBorderingCountries http response
		borderingCountryOutput, err := ioutil.ReadAll(resBorderingCountries.Body)
		if err != nil {
			http.Error(w, "Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)
			return
		}

		//Decoding JSON byte array into borderingCountries struct
		json.Unmarshal(borderingCountryOutput, &borderingCountries)

		var university []University

		//Array University struct used to save all the universities in one struct
		var universityData []University
		totalUniversities := 0

		//Looping through the bordering countries to find universities in bordering countries with similar name, and adding it to universityData struct
		for i := 0; i < len(borderingCountries); i++ {

			//Invoke external request to the universities API to get data
			resUniversity, err := externalGetRequest(w, "http://universities.hipolabs.com/search?name_contains="+url.PathEscape(universityName)+"&country="+url.PathEscape(borderingCountries[i].Name["common"]))
			if err != nil {
				fmt.Errorf(err.Error())
				return
			}

			//reading data from the resUniversity http response
			universityOutput, err := ioutil.ReadAll(resUniversity.Body)
			if err != nil {
				http.Error(w, "Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)
				return
			}

			//Decoding JSON byte array into university struct
			json.Unmarshal(universityOutput, &university)

			//Due to the fact that the universities API uses the official country name for some countries instead of common,
			//we have to check if the university struct array is empty.
			//and if its empty, we need to try again but with the offical country name instead.
			if len(university) == 0 {

				//Invoke external request to the universities API to get data
				resUniversity2, err := externalGetRequest(w, "http://universities.hipolabs.com/search?name_contains="+url.PathEscape(universityName)+"&country="+url.PathEscape(borderingCountries[i].Name["official"]))
				if err != nil {
					fmt.Errorf(err.Error())
					return
				}

				//reading data from the resUniversity http response
				universityOutput2, err := ioutil.ReadAll(resUniversity2.Body)
				if err != nil {
					http.Error(w, "Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)
					return
				}

				//Decoding JSON byte array into university struct
				json.Unmarshal(universityOutput2, &university)
			}

			//Loop to add every single university info into university2 struct. This is to avoid 2D array struct format.
			for j := 0; j < len(university); j++ {

				//Checking if the total number of universities does not exceed the limit if there is a limit.
				//If totalUniversities is equal to the limit, it will stop adding universities to the universityData struct.
				if totalUniversities != limit || limitQuery == "" {
					universityData = append(universityData, university[j])
					totalUniversities++
				}
			}

		}
		//Check if there are any valid universities in the university array struct.
		if len(universityData) == 0 {
			http.Error(w, "No universities were found", http.StatusNotFound)
			return
		}

		var countryInfo []Country
		countryNames := make(map[string]string)

		//Loop to get all the country names from the universityData struct
		//and add it to the countryNames map. Using map to avoid duplicate country names.
		for j := 0; j < len(universityData); j++ {
			countryNames[universityData[j].Country] = universityData[j].Country

		}

		//Converting the values in countryNames map to a string
		allCountryNames := convertMapToString(countryNames)

		//Invoke external request to the restcountries API to get data
		resCountry2, err := externalGetRequest(w, "https://restcountries.com/v3.1/name/"+url.PathEscape(allCountryNames))
		if err != nil {
			fmt.Errorf(err.Error())
			return
		}

		//reading data from the resCountry2 http response
		countryOutput2, err := ioutil.ReadAll(resCountry2.Body)
		if err != nil {
			http.Error(w, "Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)
			return
		}
		defer resCountry2.Body.Close()

		//Decoding JSON byte array into country2 array struct
		json.Unmarshal(countryOutput2, &countryInfo)

		fmt.Println(countryInfo)
		//Loop through the universityData array struct and countryInfo array struct to add the corresponding country-information to the university
		for i := 0; i < len(universityData); i++ {
			for k := 0; k < len(countryInfo); k++ {
				//Checks if the country name from the universityData is the same as the country name in countryInfo
				if universityData[i].Country == countryInfo[k].Name["common"] {
					//Adding the map information from the countryInfo struct to the universityData struct
					universityData[i].Maps = countryInfo[k].Maps["openStreetMaps"]

					//Adding the language information from the countryInfo struct to the universityData struct
					universityData[i].Languages = countryInfo[k].Languages
				}
			}
		}

		output, err := json.Marshal(universityData)
		fmt.Fprintf(w, string(output))

		break
	default:
		http.Error(w, "Not implemented", http.StatusNotImplemented)
	}

}
