package handler

import "time"

//AppVersion
const AppVersion = "v1"

//time in seconds from the last service restart
var StartTime = time.Now()

//URL Paths
const UNIINFO_PATH = "/unisearcher/" + AppVersion + "/uniinfo/"
const DIAG_PATH = "/unisearcher/" + AppVersion + "/diag/"
const NEIGHBOURUNIS_PATH = "/unisearcher/" + AppVersion + "/neighbourunis/"
