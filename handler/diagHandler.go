package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

func DiagHandler(w http.ResponseWriter, r *http.Request) {
	//Prepare response header
	w.Header().Add("content-type", "application/json")
	//Invoke external request to the university API to get the data
	var diag Diag

	//http status code for universities API

	resUniversityAPI, _ := http.Get("http://universities.hipolabs.com/")
	diag.Universitiesapi = resUniversityAPI.StatusCode

	//http status code for restcountries API
	resCountryAPI, _ := http.Get("https://restcountries.com/")
	diag.Countriesapi = resCountryAPI.StatusCode

	//Version
	diag.Version = AppVersion

	//time in seconds from the last service restart
	uptime := time.Since(StartTime).Seconds()
	diag.Uptime = uptime

	output, _ := json.Marshal(diag)
	fmt.Fprintf(w, string(output))

}
