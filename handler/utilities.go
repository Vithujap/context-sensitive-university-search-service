package handler

import (
	"bytes"
	"fmt"
	"net/http"
)

func externalGetRequest(w http.ResponseWriter, url string) (*http.Response, error) {
	//Request to the external  API to get the data
	r2, err := http.NewRequest(http.MethodGet, url, nil)

	if err != nil {
		http.Error(w, "Error in creating request. Error code: "+err.Error(), http.StatusInternalServerError)
		return nil, err
	}

	// Instantiate the client
	client := &http.Client{}
	//Issue request
	res, err := client.Do(r2)
	if err != nil {
		http.Error(w, "Error in response. Error message: "+err.Error(), res.StatusCode)
		return nil, err
	}

	return res, nil
}
//Code was copied from https://stackoverflow.com/questions/48149969/converting-map-to-string-in-golang
func convertMapToString(m map[string]string) string {
	count := 0
	split := ","
	b := new(bytes.Buffer)
	for _, value := range m {
		if count == len(m)-1 {
			split = ""
		}
		fmt.Fprintf(b, value+split)
		count++
	}
	return b.String()
}
