package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

func UniInfoHandler(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodGet:
		//Prepare response header
		w.Header().Add("content-type", "application/json")

		//Splitting up the URL
		split := strings.Split(r.URL.Path, "/")

		//Check if the length of the split is 5. If it is, we add an empty string to the split so that the bad request (next if-statement) dont go out of bounds
		//This is to ensure that the url format can have a "/" after {:partial_or_complete_university_name}
		if len(split) == 5 {
			split = append(split, "")
		}

		//Check for bad requests. Bad request means incorrect path format or an empty university name
		if !(len(split) > 4 && (len(split) <= 6 && split[5] == "")) || split[4] == "" {
			http.Error(w, "Expecting format .../unisearcher/v1/uniinfo/{:partial_or_complete_university_name}/", http.StatusBadRequest)
			return

		}

		universityName := split[4]

		//Invoke external request to the university API to get the data
		resUniversity, err := externalGetRequest(w, "http://universities.hipolabs.com/search?name_contains="+url.PathEscape(universityName))
		if err != nil {
			fmt.Errorf(err.Error())
			return
		}

		//reading data from the resUniversity http response
		universityOutput, err := ioutil.ReadAll(resUniversity.Body)
		if err != nil {
			http.Error(w, "Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)
			return
		}
		defer resUniversity.Body.Close()

		var university []University

		//Decoding JSON byte array into university struct
		json.Unmarshal(universityOutput, &university)

		//Check if there are any valid universities in the university array struct.
		if len(university) == 0 {
			http.Error(w, "No universities were found", http.StatusNotFound)
			return
		}

		var countryInfo []Country
		countryNames := make(map[string]string)

		//Loop to get all the country names and add it to the countryNames map. Using map to avoid duplicate country names.
		for j := 0; j < len(university); j++ {
			countryNames[university[j].Country] = university[j].Country
		}

		//Converting the values in countryNames map to a string
		allCountryNames := convertMapToString(countryNames)

		resCountry, err := externalGetRequest(w, "https://restcountries.com/v3.1/name/"+url.PathEscape(allCountryNames))
		if err != nil {
			fmt.Errorf(err.Error())
			return
		}

		//reading data from the resCountry http response
		countryOutput, err := ioutil.ReadAll(resCountry.Body)
		if err != nil {
			http.Error(w, "Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)
			return
		}
		defer resCountry.Body.Close()

		//Decoding JSON byte array into country struct
		json.Unmarshal(countryOutput, &countryInfo)

		//Loop through the university array struct and countryInfo array struct to add the corresponding country-information to the university
		for i := 0; i < len(university); i++ {
			for k := 0; k < len(countryInfo); k++ {
				//Checks if the country name from the university is the same as the country name in countryInfo
				if university[i].Country == countryInfo[k].Name["common"] {
					//Adding the map information from the countryInfo struct to the university struct
					university[i].Maps = countryInfo[k].Maps["openStreetMaps"]

					//Adding the language information from the countryInfo struct to the university struct
					university[i].Languages = countryInfo[k].Languages
				}

			}
		}
		output, err := json.Marshal(university)

		fmt.Fprintf(w, string(output))
		break
	default:
		http.Error(w, "Not implemented", http.StatusNotImplemented)
	}

}
