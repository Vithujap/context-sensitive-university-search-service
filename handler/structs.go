package handler

type University struct {
	Name           string            `json:"name"`
	Country        string            `json:"country"`
	Alpha_two_code string            `json:"alpha_two_code"`
	Web_pages      []string          `json:"web_pages"`
	Languages      map[string]string `json:"languages"`
	Maps           string            `json:"maps"`
}

type Country struct {
	Name      map[string]string `json:"name"`
	Languages map[string]string `json:"languages"`
	Maps      map[string]string
	Borders   []string `json:"borders"`
}

type Diag struct {
	Universitiesapi int     `json:"universitiesapi"`
	Countriesapi    int     `json:"countriesapi"`
	Version         string  `json:"version"`
	Uptime          float64 `json:"uptime"`
}
